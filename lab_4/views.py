from django.shortcuts import render, redirect
from django.http.response import HttpResponse

from .forms import NoteForm
from lab_2.models import Note

def index(request):
  response = {'notes': Note.objects.all()}
  return render(request, 'lab4_index.html', response)

def add_note(request):
  if request.method == 'POST':
    form = NoteForm(request.POST)
    if form.is_valid():
      form.save()
      return redirect('/lab-4')

  else:
    form = NoteForm()
    return render(request, 'lab4_form.html', {'form': form})

def note_list(request):
  response = {'notes': Note.objects.all()}
  return render(request, 'lab4_note_list.html', response)
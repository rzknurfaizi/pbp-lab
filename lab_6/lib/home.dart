import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  const Home({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<Home> createState() => _HomeState();
}

class NgingetinData {
  String deadline, title, notes;
  int color;
  NgingetinData(this.deadline, this.title, this.notes, this.color);
}

class _HomeState extends State<Home> {
  List<NgingetinData> allData = [
    NgingetinData("2 days", "do TP", "fast fast", 600),
    NgingetinData("3 days", "do Lab", "heap", 500),
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(8.0),
            child: Container(
              child: Text("HOME", style: TextStyle(fontSize: 20.0)),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 16),
            child: Text("To Do List"),
          ),
          Expanded(
            child: ListView(
              shrinkWrap: true,
              padding: const EdgeInsets.all(8),
              children: List.generate(allData.length,(index){
                return Container(
                  height: 50,
                  color: Colors.yellow[allData[index].color],
                  child: Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Text(allData[index].deadline)
                      ),
                      Expanded(
                        flex: 3,
                        child: Text(allData[index].title)
                      ),
                      Expanded(
                        flex: 3,
                        child: Text(allData[index].notes)
                      ),
                    ],
                  ),
                );
              }),
            ),
          ),
        ],
      )
    );
  }
}

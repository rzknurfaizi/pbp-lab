import 'package:flutter/material.dart';
import 'package:lab_6/about.dart';
import 'package:lab_6/home.dart';
import 'reminder.dart';
import 'newreminder.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ngingetin',
      theme: ThemeData(
        primaryColor: Colors.brown,
        primarySwatch: Colors.blue,
        fontFamily: 'Georgia',
      ),
      home: const MyHomePage(title: 'Ngingetin'),
    );
  }
}

enum Section { HOME, REMINDER, NEWREMINDER, ABOUT }

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Section section = Section.HOME;

  @override
  Widget build(BuildContext context) {
    Widget pageNow = Home(title: "HOME");

    switch (section) {
      case Section.HOME:
        pageNow = Home(title: "HOME");
        break;

      case Section.REMINDER:
        pageNow = Reminder();
        break;

      case Section.NEWREMINDER:
        pageNow = NewReminder();
        break;

      case Section.ABOUT:
        pageNow = About();
        break;
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        backgroundColor: Colors.indigo.shade900,
      ),
      body: Center(child: pageNow),
      drawer: Drawer(
        child: ListView(
          children: [
            ListTile(
              title: const Text('Home'),
              onTap: () {
                setState(() {
                  section = Section.HOME;
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Reminder'),
              onTap: () {
                setState(() {
                  section = Section.REMINDER;
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('New Reminder'),
              onTap: () {
                setState(() {
                  section = Section.NEWREMINDER;
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('About'),
              onTap: () {
                setState(() {
                  section = Section.ABOUT;
                });
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}

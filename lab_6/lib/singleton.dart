class NgingetinData {
  String deadline, title, notes;
  int color;
  NgingetinData(this.deadline, this.title, this.notes, this.color);
}

class SingletonOne {
  SingletonOne._privateConstructor();

  static final SingletonOne _instance = SingletonOne._privateConstructor();

  factory SingletonOne() {
    return _instance;
  }

  static final List<NgingetinData> allData = [
    NgingetinData("2 days", "do TP", "fast fast", 600),
    NgingetinData("3 days", "do Lab", "heap", 500),
  ];

  void addData(NgingetinData data) {
    allData.add(data);
  }

  List<NgingetinData> getData() {
    return allData;
  }
}

import 'package:flutter/material.dart';

class About extends StatelessWidget {
  const About({Key? key}) : super(key: key);

  final String description =
      "Dimasa pandemi Covid-19 ini banyak orang yang harus terpaksa melakukan WFH dan juga bersekolah secara online, banyak dari mereka yang membutuhkan suatu website yang berfungsi untuk mengatur dan mengingatkan akan jadwal dan tugas mereka. Dengan Ngingetin, maka semua kebutuhan dalam mengatur jadwal akan terpenuhi.";

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(16),
          child: Column(
            children: [
              Text("About"),
              Text(description)
            ],
          ),
        ),
        Container(padding: EdgeInsets.all(8), child: Text("FEEDBACK")),
        Container(
          padding: EdgeInsets.all(8),
          child: Text("Nama"),
        ),
        Container(
          padding: EdgeInsets.all(8),
          child: TextField(
            obscureText: true,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: "Nama",
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.all(8),
          child: Text("Pesan"),
        ),
        Container(
          padding: EdgeInsets.all(8),
          child: TextField(
            obscureText: true,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Pesan',
            ),
          ),
        ),
        TextButton(
          style: ButtonStyle(
            backgroundColor:
                MaterialStateProperty.all<Color>(Colors.blue.shade400),
            foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
          ),
          onPressed: () {},
          child: Text('Submit'),
        )
      ],
    ));
  }
}

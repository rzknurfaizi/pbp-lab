import 'package:flutter/material.dart';

class NewReminder extends StatelessWidget {
  const NewReminder({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Text("CREATE NEW REMINDER"),
          Container(
            padding: EdgeInsets.all(8),
            child: Text("Title"),
          ),
          Container(
            padding: EdgeInsets.all(8),
            child: TextField(
              obscureText: true,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Title',
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            child: Text("Deadline"),
          ),
          Container(
            padding: EdgeInsets.all(8),
            child: TextField(
              obscureText: true,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: "Deadline",
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            child: Text("Notes"),
          ),
          Container(
            padding: EdgeInsets.all(8),
            child: TextField(
              obscureText: true,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Notes',
              ),
            ),
          ),
          TextButton(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(Colors.blue.shade400),
              foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
            ),
            onPressed: () { },
            child: Text('Submit'),
          )
        ],
      )
    );
  }
}

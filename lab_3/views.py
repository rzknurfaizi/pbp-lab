from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from lab_1.models import Friend
from .forms import FriendForm

@login_required(login_url='/admin/login/')
def index(request):
  friends = Friend.objects.all()
  response = {'friends': friends}
  return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
  if request.method == 'POST':
    form = FriendForm(request.POST)
    if form.is_valid():
      form.save()
      return redirect('/lab-3')

  else:
    form = FriendForm()
    return render(request, 'lab3_form.html', {'form': form})
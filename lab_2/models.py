from django.db import models

class Note(models.Model):
  note_to = models.CharField(max_length=30, default="")
  note_from = models.CharField(max_length=30, default="")
  note_title = models.CharField(max_length=30, default="")
  note_message = models.CharField(max_length=500, default="")
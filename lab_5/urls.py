from django.urls import path
from .views import get_note, index

urlpatterns = [
    path('', index, name='index'),
    path('note/<id>', get_note, name='get_note'),
]

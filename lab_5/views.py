from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers

from lab_2.models import Note

def index(request):
    response = {'notes': Note.objects.all()}
    return render(request, 'lab5_index.html', response)

def get_note(request, id):
    data = serializers.serialize('json', Note.objects.filter(pk = id))
    return HttpResponse(data, content_type="application/json")
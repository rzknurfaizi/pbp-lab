from django.shortcuts import render
from datetime import datetime, date
from .models import Friend

mhs_name = 'Rizky' 
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2000, 2, 13)
npm = 2006597701


def index(request):
    response = {'name': mhs_name,
                'age': calculate_age(birth_date.year),
                'npm': npm}
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0


def friend_list(request):
    friends = Friend.objects.all()
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)


def friend_detail(request, name):
    friend = Friend.objects.get(name=name)
    response = {'name': friend.name,
                'date_of_birth': friend.date_of_birth,
                'age': calculate_age(friend.date_of_birth.year),
                'npm': friend.npm}
    return render(request, 'friend_detail_lab1.html', response)
from django.db import models
import datetime

class Friend(models.Model):
    name = models.CharField(max_length=30, default="Empty")
    npm = models.DecimalField(max_digits=10, decimal_places=0, default=0000000000)
    date_of_birth = models.DateField(default=datetime.date.today)
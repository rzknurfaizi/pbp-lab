from django.urls import path
from .views import index, friend_list, friend_detail

urlpatterns = [
    path('', index, name='index'),
    path('friends/', friend_list, name='friends'),
    path('friend/<str:name>', friend_detail, name='friend'),
]

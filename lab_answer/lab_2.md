1. JSON vs XML\
   JSON (JavaScript Object Notation) adalah format yang berdasarkan dari JavaScript dengan bentuk key dan value.<br/>

   XML (Extensible Markup Language) adalah format lain dengan bentuk struktur tag untuk merepresentasikan data.<br/>

   Perbedaan mendasar dari kedua format ini adalah representasi data. JSON memiliki struktur objek dengan key/value yang di-wrap dengan braces ( { } ). Setiap key akan diikuti dengan colon ( : ) lalu value-nya. Setiap pasangan key/value dipisahkan oleh koma ( , ). Contoh dari bentuk data JSON adalah

   ```json
   {
     "note": {
       "to": "kazu",
       "from": "hito"
     }
   }
   ```

   <br/>
   Sedangkan pada XML strukturnya adalah tag. Setiap value akan di-wrap oleh tag (< key > value < /key >). Antara tag satu dengan yang lainnya tidak dipisahkan dengan tanda apapun. Contoh dari bentuk data XML adalah

   ```xml
   <note>
      <to>kazu</to>
      <from>hito</from>
   </note>
   ```

   <br/>
   Perbandingan lainnya yaitu:
   <table>
    <tr>
      <th>JSON</th>
      <th>XML</th>
    <tr>
    <tr>
      <th>Tidak support array</th>
      <th>Support array</th>
    <tr>
    <tr>
      <th>Kurang secure</th>
      <th>Lebih secure</th>
    <tr>
    <tr>
      <th>Support UTF-8 encoding</th>
      <th>Support berbagai macam encoding</th>
    <tr>
   </table>

&nbsp;

2. HTML vs XML<br/>
   HTML (Hypertext Markup Language) adalah sebuah markup language untuk menampilkan dokumen pada web browser. Markup language sendiri adalah bahasa yang digunakan untuk melakukan format terhadap konten dokumen secara visual.<br/>

   Definisi dari XML sendiri sudah dijelaskan pada nomor sebelumnya. Perbedaan utama keduanya terletak pada penggunaannya. HTML digunakan untuk melakukan format visual terhadap dokumen pada browser, sedangkan XML digunakan untuk menstruktur dan menyimpan suatu data. Perbedaan lainnya adalah tag pada html tidak akan ditampilkan di browser karena tag tersebut bertujuan untuk memberitahu browser tampilan visual yang harus dimunculkan dari konten dokumen. Sedangkan tag pada xml akan ditampilkan sebagai key dari data.
